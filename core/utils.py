from django.db import models


class ListField(models.TextField):

    def __init__(self, separator=',', *args, **kwargs):
        self.separator = separator
        super().__init__(*args, **kwargs)

    def from_db_value(self, value, expression, connection):
        if value is None:
            return value
        return value.split(self.separator)

    def to_python(self, value):
        if isinstance(value, list):
            return value
        if value is None:
            return value
        return value.split(self.separator)

    def get_prep_value(self, value):
        return self.separator.join([str(val) for val in value])

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        if self.separator != ',':
            kwargs['separator'] = self.separator
        return name, path, args, kwargs
