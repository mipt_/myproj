from django.contrib.auth import get_user_model

from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import (
    DjangoModelPermissionsOrAnonReadOnly,
    IsAuthenticated
)
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_400_BAD_REQUEST
)

from .serializers import UserSerializer, ChangePasswordSerializer

UserModel = get_user_model()


class UserViewSet(ModelViewSet):
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)
    queryset = UserModel.objects.all()
    serializer_class = UserSerializer

    @action(methods=['put'],
            detail=False,
            url_name='change_password',
            url_path='change_password',
            permission_classes=[IsAuthenticated])
    def change_password(self, request, *args, **kwargs):
        serializer = ChangePasswordSerializer(user=request.user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.validated_data, status=HTTP_200_OK)
        else:
            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)
