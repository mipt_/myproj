from django.contrib.auth import get_user_model, password_validation
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

UserModel = get_user_model()


class SetPasswordSerializer(serializers.Serializer):
    new_password1 = serializers.CharField(max_length=128, write_only=True)
    new_password2 = serializers.CharField(max_length=128, write_only=True)

    def validate_new_password2(self, value):
        password1 = self.initial_data.get('new_password1')
        if password1 and value:
            if password1 != value:
                raise serializers.ValidationError(_('Passwords not match'))
        password_validation.validate_password(value)
        return value


class UserSerializer(serializers.ModelSerializer, SetPasswordSerializer):
    id = serializers.IntegerField(required=False)
    username = serializers.CharField(max_length=150, required=True, trim_whitespace=True)
    email = serializers.EmailField(required=True)
    new_password1 = serializers.CharField(max_length=128, write_only=True, required=False)
    new_password2 = serializers.CharField(max_length=128, write_only=True, required=False)

    def validate_username(self, val):
        try:
            if UserModel.objects.get(username=val):
                raise serializers.ValidationError(_('Username *%s* already exists') % val)
        except UserModel.DoesNotExist:
            pass
        return val

    def create(self, validated_data):
        user = UserModel.objects.create(**validated_data)
        if validated_data.get('new_password2'):
            user.set_password(validated_data['new_password2'])
            user.save()
        return user

    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.email = validated_data.get('email', instance.email)
        if validated_data.get('new_password2'):
            instance.set_password(validated_data['new_password2'])
        instance.save()
        return instance

    class Meta:
        model = UserModel
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'new_password1',
            'new_password2',
        )


class ChangePasswordSerializer(SetPasswordSerializer):
    old_password = serializers.CharField(max_length=128, write_only=True)

    def __init__(self, user, data=None, **kwargs):
        self.user = user
        super().__init__(user, data, **kwargs)

    def validate_old_password(self, value):
        if not self.user.check_password(value):
            raise serializers.ValidationError(_('Invalid password'))
        return value

    def save(self, **kwargs):
        password = self.validated_data["new_password1"]
        self.user.set_password(password)
        self.user.save()
        return self.user
