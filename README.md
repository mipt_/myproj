# MyProj
competition app

## Table of Contents

* [Requirements](#requirements)
* [Installation](#installation)
* [Run](#run)
* [API](#api)
    * [Auth (token)](#auth)
        * [Obtain token](#obtain-token)
        * [Refresh token](#refresh-token)
    * [User](#user)
        * [Show users](#show-users)
        * [Show user](#show-user)
        * [Create user](#create-user)

### Requirements

These are required to pre-install:

- Python 3.x
- Django 3.x
- Virtualenv (optional)

### Installation

Run the following scripts to install:

```bash
cd ./myproj
python manage.py migrate
```

### Run

To start development server, run the following script:
```bash
python manage.py runserver 8000
```

### API

### Auth

#### Obtain token

Used to obtain a Token for a registered User.

**URL**: `/api/auth/token`

**Method**: `POST`

**Auth required**: NO

**Data constraints**:

```json
{
    "username": "[username]",
    "password": "[password in plain text]"
}
```

**Signature**:

```
username - required, max_length=150
password - required, max_length=128
```

##### Success response

**Code**: `200 OK`

**Content**:

```json
{
    "refresh": "[refresh token]",
    "access": "[access token]"
}
```

##### Error response: Unauthorized

**Condition**: If auth-token not provided

**Code**: `401 Unauthorized`

**Content**:

```json
{
    "detail": "No active account found with the given credentials"
}
```

##### Error response: BadRequest

**Condition**: If 'username' and 'password' combination is wrong.

**Code**: `400 BadRequest`

**Content example**:

```json
{
    "username": [
        "This field is required."
    ],
    "password": [
        "This field is required."
    ]
}
```

#### Refresh token

When this short-lived access token expires, 
you can use the longer-lived refresh token to obtain another access token

**URL**: `/api/token/refresh`

**Method**: `POST`

**Auth required**: NO

**Data constraints**: 

```json
{
	"refresh": "[refresh token]"
}
```

**Signature**:

```
refresh - required
```

##### Success response

**Code**: `200 OK`

**Content**:

```json
{
    "access": "[access token]"
}
```

##### Error response: Unauthorized

**Condition**: If auth-token not provided

**Code**: `401 Unauthorized`

**Content example**:

```json
{
    "detail": "Token is invalid or expired",
    "code": "token_not_valid"
}
```

##### Error response: BadRequest

**Condition**: If 'refresh' is invalid.

**Code**: `400 BadRequest`

**Content example**:

```json
{
    "refresh": [
        "This field is required."
    ]
}
```

### User

#### Show users

Show accessible users

**URL**: `/api/user`

**Method**: `GET`

**Auth required**: NO

**Data constraints**: 

```json
{}
```

##### Success response

**Code**: `200 OK`

**Content example**:

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "username": "admin",
            "first_name": "",
            "last_name": "",
            "email": ""
        }
    ]
}
```

#### Show user

Show single user

**URL**: `/api/user/:pk/`

**URL Parameters**: `pk=[integer]` where `pk` is the ID of the user

**Method**: `GET`

**Auth required**: NO

**Data constraints**: 

```json
{}
```

##### Success response

**Code**: `200 OK`

**Content example**:

```json
{
    "id": 1,
    "username": "random",
    "first_name": "",
    "last_name": "",
    "email": ""
}
```

##### Error response: NotFound

**Condition**: If user does not exist with `id` of provided `pk` parameter

**Code**: `404 NotFound`

**Content**:

```json
{
    "detail": "Not found."
}
```


#### Create user

**URL**: `/api/user/`

**Method**: `POST`

**Auth required**: NO

**Data constraints**: 

```json
{
    "username": "",
    "first_name": "",
    "last_name": "",
    "email": "",
    "new_password1":"",
    "new_password2":""
}
```

**Signature**:

```
username - string, max_length=150, required
first_name - string, max_length=30, optional
last_name - string, max_length=150, optional
email - string, max_length=254, required
new_password1 - string, max_length=128, optional
new_password2 - string, max_length=128, optional
```

##### Success response

**Code**: `201 Created`

**Content example**:

```json
{
    "id": 1,
    "username": "admin",
    "email": ""
}
```

##### Error response: BadRequest

**Condition**: If `username` is invalid

**Code**: `400 BadRequest`

**Content example**:

```json
{
    "username": [
        "This field is required."
    ]
}
```
